# NetatScreen
Python script used to retrieve data from your Netatmo Weather Station and display it on a LCD screen connected to a Raspberry Pi.

## Usage
 * Retrieve API credentials from [Netatmo](http://dev.netatmo.com/doc/)
 * Rename the `.credentials_example` file in `.credentials`
 * Put your API credentials in the `.credentials` file
 * Change the name of the external sensor in `netatScreen.py` (defaults to `Fenêtre`)
 * Connect the LCD to your Raspberry and set the GPIO in `netatScreen.py` according to your setup
 * Run `netatScreen.py`

A cron is also available in the `netatScreenCron` file: it fetches the data and update the display every 5 minutes. You can copy it in `/etc/cron.d/`.

## Documentation
This script is based on:
* The `lnetatmo` package to retrieve the data from Netatmo, available on [GitHub](https://github.com/philippelt/netatmo-api-python). The package documentation is available [here](https://github.com/philippelt/netatmo-api-python/blob/master/usage.md).
* The `RPLCD` package to display the data on the LCD screen, available on [GitHub](https://github.com/dbrgn/RPLCD). The package documentation is available [here](https://rplcd.readthedocs.io/en/stable/).
