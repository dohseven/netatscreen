#!/usr/bin/python3
# encoding=utf-8

import os
import json
import lnetatmo
from RPLCD.gpio import CharLCD
from RPi        import GPIO

# Credentials are stored as a JSON file next to this script
credentials = os.path.dirname(os.path.realpath(__file__)) + '/.credentials'
with open(credentials, 'r') as jsonCreds:
    creds = json.load(jsonCreds)
    jsonCreds.close()

    # Connect to Netatmo server and retrieve data
    authorization = lnetatmo.ClientAuth(clientId = creds['CLIENT_ID'],
                                        clientSecret = creds['CLIENT_SECRET'],
                                        username = creds['USERNAME'],
                                        password = creds['PASSWORD'])
    devList = lnetatmo.WeatherStationData(authorization)
    devData = devList.lastData()

    # Initialize screen and clear it
    # Set the GPIO according to your setup
    lcd = CharLCD(pin_rs=17, pin_e=18, pins_data=[22, 23, 24, 25],
                  numbering_mode=GPIO.BCM,
                  cols=16, rows=2,
                  charmap='A02')
    lcd.clear()

    # Write data on screen
    degre = (0b00110,
             0b01001,
             0b01001,
             0b00110,
             0b00000,
             0b00000,
             0b00000,
             0b00000)
    lcd.create_char(0, degre)
    lcd.write_string('Int: %4s\x00C %-4s' % (devData['Indoor']['Temperature'], devData['Indoor']['CO2']))
    lcd.cursor_pos = (1, 0)
    # Make sure that the name of the external sensor is correctly set
    lcd.write_string('Ext: %4s\x00C' % devData['Fenêtre']['Temperature'])

    # Clean-up
    lcd.close()
